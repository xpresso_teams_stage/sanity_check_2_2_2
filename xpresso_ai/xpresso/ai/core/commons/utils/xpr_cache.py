from collections import deque
from sys import getsizeof
from threading import RLock


class XprCache:
    """
    base class to generate a cache object on xpresso platform
    """
    def __init__(self, max_length, max_size):
        """
        constructor for XprCache Object
        Args:
            max_length: maximum length for entries to be saved in cache
            max_size: maximum size of cache
        """
        self.max_length = max_length
        self.max_size = max_size
        self._queue = deque()
        self._cache_info = dict()
        self.lock = RLock()

    def set(self, key: str, val):
        """
        add a new entry to the cache if not present
        Args:
            key: key of the entry to be added
            val:  value of the entry to be saved
        """
        new_entry_size = getsizeof(val)
        with self.lock:
            print(f"\n setting {key}\n")
            if key in self._cache_info:
                self.update_queue_for_hit(key)
                self._cache_info[key] = val
                return
            self.reduce_cache_for_size(new_entry_size)
            if len(self._queue) == self.max_length:
                self.remove_lru_entry()
            self._cache_info[key] = val
            self._queue.appendleft(key)

    def remove_lru_entry(self):
        """
        remove an entry from the cache as per lru logic
        """
        with self.lock:
            key = self._queue.pop()
            print(f"removing an entry from cache: \n {key}\n")
            del self._cache_info[key]

    def reduce_cache_for_size(self, new_entry_size):
        """
        Modifying the cache object in order to reduce the size below max_size
        Args:
            new_entry_size: size of new entry item that needs to be added
        """
        with self.lock:
            while new_entry_size + getsizeof(self._cache_info) > self.max_size:
                self.remove_lru_entry()

    def delete_all(self):
        """
        deletes all the entries from deque and cache
        """
        with self.lock:
            self._queue.clear()
            self._cache_info.clear()

    def get(self, key):
        """
        fetch value of an entry from cache
        Args:
            key: entry key to fetch the value for
        Returns:
             value of the entry saved in cache
        """
        with self.lock:
            if key not in self._cache_info:
                return None
            self.update_queue_for_hit(key)
            return self._cache_info[key]

    def _get_all(self):
        """
        returns the complete cache_info saved
        """
        with self.lock:
            return self._cache_info

    def update_queue_for_hit(self, key):
        """
        updates the queue after a new hit of any item
        Args:
            key: key of the item that has been hit
        """
        with self.lock:
            key_index = self._queue.index(key)
            del self._queue[key_index]
            self._queue.appendleft(key)
